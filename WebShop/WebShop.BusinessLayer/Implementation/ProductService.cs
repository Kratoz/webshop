﻿using System;
using System.Collections.Generic;
using WebShop.BusinessLayer.Interfaces;
using WebShop.DataLayer.Interfaces;
using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Implementation
{
    public class ProductService : IProductService
    {
        private readonly IProductStorage productstorage;

        public ProductService(IProductStorage storage)
        {
            productstorage = storage;
        }

        public void AddProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public void DeleteProduct(int productId)
        {
            throw new NotImplementedException();
        }

        public void EditProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetCatalog()
        {
            return productstorage.GetCatalog();
        }

        public Product GetProduct(int id)
        {
            return productstorage.GetProduct(id);
        }
    }
}
