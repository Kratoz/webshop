﻿using System;
using WebShop.BusinessLayer.Interfaces;
using WebShop.DataLayer.Interfaces;
using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserStorage userStorage;

        public UserService(IUserStorage storage)
        {
            userStorage = storage;
        }
        
        public User LogIn(string username)
        {
            return userStorage.GetUserByLogin(username);
        }

        public void RegisterUser(string username, string password)
        {
            var user = new User
            {
                Login = username,
                Password = password,
                Role = UserRole.User,
            };

            userStorage.AddUser(user);
        }
    }
}
