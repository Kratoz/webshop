﻿using System;
using System.Collections.Generic;
using WebShop.BusinessLayer.Interfaces;
using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Implementation
{
    public class CartService : ICartService
    {
        public void AddProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public void CheckOut(User user, string ShippingAdress)
        {
            throw new NotImplementedException();
        }

        public void EditProductAmount(int productId, int newAmount)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetCart()
        {
            throw new NotImplementedException();
        }

        public void RemoveProduct(int productId)
        {
            throw new NotImplementedException();
        }
    }
}
