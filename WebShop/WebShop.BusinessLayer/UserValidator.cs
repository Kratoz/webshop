﻿using WebShop.DataLayer.Interfaces;

namespace WebShop.BusinessLayer
{
    public class UserValidator
    {
        private readonly IUserStorage userStorage;

        public UserValidator(IUserStorage storage)
        {
            userStorage = storage;
        }

        public bool LoginValidate(string login)
        {
            if (userStorage.GetAllLogins().Exists(item => item == login))
            {
                return false;
            }

            return true;
        }

        public bool CheckUserInfo(string login, string password)
        {
            return userStorage.CheckUserInfo(login, password);
        }
    }
}
