﻿using System.Collections.Generic;
using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Interfaces
{
    public interface IProductService
    {
        void AddProduct(Product product);

        void EditProduct(Product product);

        void DeleteProduct(int productId);

        IEnumerable<Product> GetCatalog();

        Product GetProduct(int id);
    }
}
