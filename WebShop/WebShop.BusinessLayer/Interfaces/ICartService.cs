﻿using System.Collections.Generic;
using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Interfaces
{
    public interface ICartService
    {
        void AddProduct(Product product);

        void CheckOut(User user, string ShippingAdress);

        IEnumerable<Product> GetCart();

        void RemoveProduct(int productId);

        void EditProductAmount(int productId, int newAmount);
    }
}
