﻿using WebShop.Shared.Models;

namespace WebShop.BusinessLayer.Interfaces
{
    public interface IUserService
    {
        void RegisterUser(string username, string password);

        User LogIn(string username);
    }
}
