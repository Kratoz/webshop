﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WebShop.UILayer.ViewModels
{
    public class UserRegisterViewModel
    {
        [Required]
        [MinLength(4)]
        [MaxLength(20)]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("ConfirmPassword")]
        [MinLength(6)]
        [MaxLength(33)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
