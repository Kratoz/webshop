﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.UILayer.ViewModels
{
    public class UserLoginViewModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
