﻿using Microsoft.AspNetCore.Mvc;
using WebShop.BusinessLayer.Interfaces;

namespace WebShop.UILayer.Controllers
{
    public class CatalogController : Controller
    {
        private readonly IProductService productService;

        public CatalogController(IProductService service)
        {
            productService = service;
        }

        public IActionResult Index()
        {
            return View(productService.GetCatalog());
        }
    }
}
