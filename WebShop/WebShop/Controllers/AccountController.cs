﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebShop.BusinessLayer;
using WebShop.BusinessLayer.Interfaces;
using WebShop.UILayer.ViewModels;

namespace WebShop.UILayer.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userSerivce;

        private readonly UserValidator validator;

        public AccountController(IUserService serivce, UserValidator validator)
        {
            this.validator = validator;
            userSerivce = serivce;
        }
        
        // GET: AccountController
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(UserLoginViewModel model)
        {
            if (!validator.CheckUserInfo(model.Login, model.Password))
            {
                ModelState.AddModelError("Login", "Неправильный логин или пароль");
                return View(model);
            }

            var user = userSerivce.LogIn(model.Login);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Role, user.Role.ToString()),
                new Claim(type: "Id", value: user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Login),
            };

            var claimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimIdentity));

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Register(UserRegisterViewModel model)
        {
            if (!validator.LoginValidate(model.Login))
            {
                ModelState.AddModelError("Login", "Имя пользователя занято");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            userSerivce.RegisterUser(model.Login, model.Password);

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index", "Home");
        }
    }
}
