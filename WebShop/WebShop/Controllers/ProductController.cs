﻿using Microsoft.AspNetCore.Mvc;
using WebShop.BusinessLayer.Interfaces;

namespace WebShop.UILayer.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService productService;

        public ProductController(IProductService service)
        {
            productService = service;
        }

        public IActionResult Index(int id)
        {
            return View(productService.GetProduct(id));
        }
    }
}
