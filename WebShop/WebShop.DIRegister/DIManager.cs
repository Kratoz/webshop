﻿using Microsoft.Extensions.DependencyInjection;
using WebShop.BusinessLayer.Implementation;
using WebShop.BusinessLayer.Interfaces;
using WebShop.DataLayer;
using WebShop.DataLayer.Implementation;
using WebShop.DataLayer.Interfaces;
using WebShop.BusinessLayer;

namespace WebShop.DIRegister
{
    public static class DIManager
    {
        public static void AddDI(this IServiceCollection services)
        {
            services.AddSingleton<DBOptions>();
            services.AddSingleton<IUserStorage, UserStorage>();
            services.AddSingleton<IProductStorage, ProductStorage>();
            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<UserValidator>();
        }
    }
}
