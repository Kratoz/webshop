﻿namespace WebShop.Shared.Models
{
    public class UserData
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public int? PhoneNumber { get; set; }

        public string Address { get; set; }
    }
}
