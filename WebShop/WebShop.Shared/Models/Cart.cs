﻿using System.Collections.Generic;

namespace WebShop.Shared.Models
{
    public class Cart
    {
        public List<Product> PurchaseList { get; }

        public decimal PriceTotal { get; set; }

        public int Discount { get; set; }
    }
}
