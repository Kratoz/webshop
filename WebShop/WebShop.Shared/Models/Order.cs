﻿using System;

namespace WebShop.Shared.Models
{
    public class Order : Cart
    {
        public int OrderId { get; }

        public User Customer { get; }

        public string ShippingAddress { get; set; }

        public DateTime Date { get; }

        public string Status { get; set; }

    }
}
