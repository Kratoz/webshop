﻿namespace WebShop.Shared.Models
{
    public enum UserRole
    {
        User = 1,
        Editor = 2,
        Admin = 3,
    }
}
