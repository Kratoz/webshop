﻿namespace WebShop.Shared.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public UserRole Role { get; set; }

        public UserData UserData { get; set; }
    }
}
