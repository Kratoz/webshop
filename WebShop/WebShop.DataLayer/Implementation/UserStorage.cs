﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebShop.DataLayer.Interfaces;
using WebShop.Shared.Models;

namespace WebShop.DataLayer.Implementation
{
    public class UserStorage : IUserStorage
    {
        private readonly DBOptions options;

        public UserStorage(DBOptions options)
        {
            this.options = options;
        }

        public void AddUser(User user)
        {
            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "INSERT INTO [User] VALUES(@Login, @Password, @Role)";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("Login", DbType.String)
                {
                    Value = user.Login,
                });
                command.Parameters.Add(new SqlParameter("Password", DbType.String)
                {
                    Value = user.Password,
                });
                command.Parameters.Add(new SqlParameter("Role", DbType.Int32)
                {
                    Value = (int)user.Role,
                });

                command.ExecuteNonQuery();
            }
        }

        public void EditPassword(int userId, string passwrod)
        {
            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "UPDATE User SET Password = @Pass WHERE Id = @Userid";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("Userid", DbType.Int32)
                {
                    Value = userId,
                });
                command.Parameters.Add(new SqlParameter("Pass", DbType.String)
                {
                    Value = passwrod,
                });

                command.ExecuteNonQuery();
            }
        }

        public void EditUserData(int id, string email = null, int? number = null, string address = null, string name = null, string surname = null)
        {
            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "UPDATE UserData SET Email = @email, PhoneNumber = @number, Address = @address, name = @name, surname = @surname  WHERE Id = @Userid";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("email", DbType.String)
                {
                    Value = email,
                });
                command.Parameters.Add(new SqlParameter("number", DbType.Int32)
                {
                    Value = number,
                });
                command.Parameters.Add(new SqlParameter("address", DbType.String)
                {
                    Value = address,
                });
                command.Parameters.Add(new SqlParameter("name", DbType.String)
                {
                    Value = name,
                });
                command.Parameters.Add(new SqlParameter("surname", DbType.String)
                {
                    Value = surname,
                });
                command.Parameters.Add(new SqlParameter("Userid", DbType.Int32)
                {
                    Value = id,
                });

                command.ExecuteNonQuery();
            }
        }

        public UserData GetUserData(int userId)
        {
            var userData = new UserData();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT * FROM UserData WHERE Id = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("userId", userId));

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            userData.Email = reader["Email"].ToString();
                            userData.PhoneNumber = Convert.ToInt32(reader["PhoneNumber"]);
                            userData.Address = reader["Address"].ToString();
                            userData.Name = reader["Name"].ToString();
                            userData.Surname = reader["Surname"].ToString();
                        }
                    }
                }
            }

            return userData;
        }

        public List<string> GetAllLogins()
        {
            var logins = new List<string>();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT Login FROM [User]";
                var command = new SqlCommand(query, connection);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            logins.Add(reader["Login"].ToString());
                        }
                    }
                }
            }

            return logins;
        }

        public bool CheckUserInfo(string login, string password)
        {
            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT COUNT(Login) FROM [User] WHERE Login = @login AND Password = @password";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("login", login));
                command.Parameters.Add(new SqlParameter("password", password));

                if ((int)command.ExecuteScalar() != 0)
                {
                    return true;
                }

                return false;
            }
        }

        public User GetUserByLogin(string login)
        {
            var user = new User();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT * FROM [User] WHERE Login = @Login";
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("Login", login));

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            user.Login = reader["Login"].ToString();
                            user.Id = Convert.ToInt32(reader["Id"]);
                            user.Role = (UserRole)Convert.ToInt32(reader["Role"]);
                        }
                    }
                }
            }

            return user;
        }

        public bool LogIn(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}
