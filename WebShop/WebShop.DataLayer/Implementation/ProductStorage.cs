﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebShop.DataLayer.Interfaces;
using WebShop.Shared.Models;

namespace WebShop.DataLayer.Implementation
{
    public class ProductStorage : IProductStorage
    {
        private readonly DBOptions options;

        public ProductStorage(DBOptions options)
        {
            this.options = options;
        }

        public List<Product> GetCatalog()
        {
            var catalog = new List<Product>();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT * FROM Products";
                var command = new SqlCommand(query, connection);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            catalog.Add(new Product
                            {
                                Title = reader["Title"].ToString(),
                                Price = Convert.ToDecimal(reader["Price"]),
                                Count = Convert.ToInt32(reader["Count"]),
                                Description = reader["Description"].ToString(),
                                Id = Convert.ToInt32(reader["Id"]),
                                Category = Convert.ToInt32(reader["Category_Id"]),
                            });
                        }
                    }
                }
            }

            return catalog;
        }

        public List<Product> GetProductByCategory(int categoryId)
        {
            var catalog = new List<Product>();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT * FROM Products WHERE Category_id = @categoryId";
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("categoryId", categoryId));

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            catalog.Add(new Product
                            {
                                Title = reader["Title"].ToString(),
                                Price = Convert.ToDecimal(reader["Price"]),
                                Count = Convert.ToInt32(reader["Count"]),
                                Description = reader["Description"].ToString(),
                                Id = Convert.ToInt32(reader["Id"]),
                                Category = Convert.ToInt32(reader["Category_Id"]),
                            });
                        }
                    }
                }
            }

            return catalog;
        }

        public void AddProduct(Product product)
        {
            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "INSERT INTO Products VALUES(@Title, @Price, @Category, @Count, @Image)";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("Title", DbType.String)
                {
                    Value = product.Title,
                });
                command.Parameters.Add(new SqlParameter("Price", DbType.Decimal)
                {
                    Value = product.Price,
                });
                command.Parameters.Add(new SqlParameter("Category", DbType.Int32)
                {
                    Value = product.Category,
                });
                command.Parameters.Add(new SqlParameter("Count", DbType.Int32)
                {
                    Value = product.Count,
                });
                command.Parameters.Add(new SqlParameter("Image", DbType.String)
                {
                    Value = product.ImageUrl,
                });

                command.ExecuteNonQuery();
            }
        }

        public Product GetProduct(int id)
        {
            var product = new Product();

            using (var connection = new SqlConnection(options.ConnectionString))
            {
                connection.Open();

                var query = "SELECT * FROM Products WHERE Id = @id";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("id", id));

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            product = new Product
                            {
                                Title = reader["Title"].ToString(),
                                Price = Convert.ToDecimal(reader["Price"]),
                                Count = Convert.ToInt32(reader["Count"]),
                                Description = reader["Description"].ToString(),
                                Id = Convert.ToInt32(reader["Id"]),
                                Category = Convert.ToInt32(reader["Category_Id"]),
                            };
                        }
                    }
                }
            }

            return product;
        }
    }
}
