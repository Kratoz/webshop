﻿using Microsoft.Extensions.Configuration;

namespace WebShop.DataLayer
{
    public class DBOptions
    {
        public DBOptions(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public string ConnectionString { get; set; }
    }
}
