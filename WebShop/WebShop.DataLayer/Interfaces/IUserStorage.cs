﻿using System.Collections.Generic;
using WebShop.Shared.Models;

namespace WebShop.DataLayer.Interfaces
{
    public interface IUserStorage
    {
        void AddUser(User user);

        void EditPassword(int userId, string passwrod);

        bool LogIn(User user);

        void EditUserData(int id, string email = null, int? number = null, string address = null, string name = null, string surname = null);

        UserData GetUserData(int userId);

        public List<string> GetAllLogins();

        bool CheckUserInfo(string login, string password);

        User GetUserByLogin(string login);
    }
}
