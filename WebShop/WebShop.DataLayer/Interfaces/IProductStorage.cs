﻿using System.Collections.Generic;
using WebShop.Shared.Models;

namespace WebShop.DataLayer.Interfaces
{
    public interface IProductStorage
    {
        List<Product> GetCatalog();

        List<Product> GetProductByCategory(int categoryId);

        void AddProduct(Product product);

        Product GetProduct(int id);
    }
}
